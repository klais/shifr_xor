﻿namespace Shifrovanie_XOR
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtEncrypted = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton_rasshifr = new System.Windows.Forms.RadioButton();
            this.radioButton_shifr = new System.Windows.Forms.RadioButton();
            this.button_save = new System.Windows.Forms.Button();
            this.button_start = new System.Windows.Forms.Button();
            this.button_load = new System.Windows.Forms.Button();
            this.KeyLength = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.KeyLength)).BeginInit();
            this.SuspendLayout();
            // 
            // txtEncrypted
            // 
            this.txtEncrypted.Location = new System.Drawing.Point(828, 46);
            this.txtEncrypted.Multiline = true;
            this.txtEncrypted.Name = "txtEncrypted";
            this.txtEncrypted.Size = new System.Drawing.Size(760, 547);
            this.txtEncrypted.TabIndex = 7;
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(50, 733);
            this.txtKey.Multiline = true;
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(191, 47);
            this.txtKey.TabIndex = 6;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(12, 46);
            this.txtContent.Multiline = true;
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(786, 547);
            this.txtContent.TabIndex = 3;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(290, 733);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(244, 47);
            this.button1.TabIndex = 9;
            this.button1.Text = "сгенерировать ключ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton_rasshifr
            // 
            this.radioButton_rasshifr.AutoSize = true;
            this.radioButton_rasshifr.Location = new System.Drawing.Point(1350, 733);
            this.radioButton_rasshifr.Name = "radioButton_rasshifr";
            this.radioButton_rasshifr.Size = new System.Drawing.Size(192, 29);
            this.radioButton_rasshifr.TabIndex = 10;
            this.radioButton_rasshifr.TabStop = true;
            this.radioButton_rasshifr.Text = "расшифровать";
            this.radioButton_rasshifr.UseVisualStyleBackColor = true;
            this.radioButton_rasshifr.CheckedChanged += new System.EventHandler(this.radioButton_rasshifr_CheckedChanged);
            // 
            // radioButton_shifr
            // 
            this.radioButton_shifr.AutoSize = true;
            this.radioButton_shifr.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButton_shifr.Location = new System.Drawing.Point(1021, 733);
            this.radioButton_shifr.Name = "radioButton_shifr";
            this.radioButton_shifr.Size = new System.Drawing.Size(180, 29);
            this.radioButton_shifr.TabIndex = 11;
            this.radioButton_shifr.TabStop = true;
            this.radioButton_shifr.Text = "зашифровать";
            this.radioButton_shifr.UseVisualStyleBackColor = true;
            this.radioButton_shifr.CheckedChanged += new System.EventHandler(this.radioButton_shifr_CheckedChanged);
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(1350, 638);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(238, 61);
            this.button_save.TabIndex = 12;
            this.button_save.Text = "сохранить текст";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(993, 638);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(208, 61);
            this.button_start.TabIndex = 13;
            this.button_start.Text = "зашифр/разшифр";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_load
            // 
            this.button_load.Location = new System.Drawing.Point(50, 638);
            this.button_load.Name = "button_load";
            this.button_load.Size = new System.Drawing.Size(193, 61);
            this.button_load.TabIndex = 14;
            this.button_load.Text = "загрузить текст";
            this.button_load.UseVisualStyleBackColor = true;
            this.button_load.Click += new System.EventHandler(this.button_load_Click);
            // 
            // KeyLength
            // 
            this.KeyLength.Location = new System.Drawing.Point(883, 492);
            this.KeyLength.Margin = new System.Windows.Forms.Padding(6);
            this.KeyLength.Name = "KeyLength";
            this.KeyLength.Size = new System.Drawing.Size(240, 31);
            this.KeyLength.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1600, 865);
            this.Controls.Add(this.button_load);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.radioButton_shifr);
            this.Controls.Add(this.radioButton_rasshifr);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtEncrypted);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.txtContent);
            this.Controls.Add(this.KeyLength);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.KeyLength)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtEncrypted;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButton_rasshifr;
        private System.Windows.Forms.RadioButton radioButton_shifr;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_load;
        private System.Windows.Forms.NumericUpDown KeyLength;
    }
}

