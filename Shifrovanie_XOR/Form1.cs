﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Shifrovanie_XOR
{
   
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
      
            radioButton_shifr.Checked = true;
            KeyLength.Value = 100;
        }

        private void button_load_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";
            if (o.ShowDialog() == DialogResult.OK)
            {
                if (radioButton_shifr.Checked) txtContent.Text = File.ReadAllText(o.FileName, Encoding.UTF8);
                if (radioButton_rasshifr.Checked) txtEncrypted.Text = File.ReadAllText(o.FileName, Encoding.UTF8);
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (radioButton_shifr.Checked)
                {
                    var streamWriter = new StreamWriter(saveFileDialog.FileName);
                    streamWriter.WriteLine(txtEncrypted.Text);
                    streamWriter.Close();
                }
                if (radioButton_rasshifr.Checked)
                {
                    var streamWriter = new StreamWriter(saveFileDialog.FileName);
                    streamWriter.WriteLine(txtContent.Text);
                    streamWriter.Close();
                }
            }
        }

        private void radioButton_shifr_CheckedChanged(object sender, EventArgs e)
        {
            txtContent.ReadOnly = false;
            txtEncrypted.ReadOnly = true;
        }

        private void radioButton_rasshifr_CheckedChanged(object sender, EventArgs e)
        {
            txtContent.ReadOnly = true;
            txtEncrypted.ReadOnly = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UInt16 size = (ushort)(KeyLength.Value);
            Char maxchar = (char)(255);
            var rand = new Random();
            string key = "";
            while (key.Length < size)
            {
                key += (char)(rand.Next(0, maxchar));
            }
            txtKey.Text = key;
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            string local_key = txtKey.Text, result = "", rab = "";
            if (radioButton_shifr.Checked) rab = txtContent.Text;
            if (radioButton_rasshifr.Checked) rab = txtEncrypted.Text;
            while (rab.Length % local_key.Length != 0)
            {
                rab += (char)(0);
            }
            for (int i = 0; i < rab.Length; i++)
            {
                result += (char)(rab[i] ^ local_key[i % local_key.Length]);
            }
            if (radioButton_shifr.Checked) txtEncrypted.Text = result;
            if (radioButton_rasshifr.Checked) txtContent.Text = result;
        }
    }
}
